# Objectives of the workshop

The objectives of this workshop is to manipulate Git-Flow on a fake project to have a better understanding of it.

The development part of the project will be deliberately trivial.

The important is that the team concentrates on its Git actions, methodology and decisions.

# Description of the project

The project is a basic HTML page, with multiple pages.

No need to do CSS, but if you want you can.

You can use any CSS library if you wish.

The projet will have a landing page (index.html), with links to other pages.

# Modalities

Project will be made by teams of 2 or 3 developers.

Each team will have a repo on Github, belonging to one of the developers of the team.

Other developers will be invited to this project.

# Steps

## Step 1

Initialize the project on git, invite your teammates.

Create branch master and branch develop.

Let every teammate clone the repo in local.

## Step 2

For the first feature, you are asked to add a page that says "hello" in French, and add a link to this page on your landing page.

No release for production has to be made with just this feature.

Take the good decisions :
- create a branch for this feature or not ?
- if yes, where to create this branch from ?
- where do you merge this branch into ?

## Step 3

For the next features, you are asked to :
- add a page that says "hello" in English
- add a page that says "hello" in Spanish
- add a page that says "hello" in Italian
- add links to this pages on your landing page

Prepare a release for production with this feature.

Take the good decisions :
- create a branch for theses features or not ?
- if yes, where to create these branches from ?
- where do you merge these branches into ?
- how to create a release ?

## Step 4

For the next features, you are asked to :
- add a page that says "hello" in Portuguese
- add a page that says "hello" in German
- add a page that says "hello" in Polish
- add links to this pages on your landing page

Prepare a release with these features, but don't put it in production yet.

## Step 5

Urgent bug fix is needed in production ! (release made in step 3)

Replace Italian page by Russian page, and replace the link on landing page.

Take the good decisions :
- how to create a hotfix ?
- where to merge it ?

## Step 6

While the release prepared in step 4 is still not in production, we start working on next release.

For the next features, you are asked to :
- add a page that says "hello" in Dutch
- add a link to this page on your landing page

## Step 7

A mandatory improvement on the released prepared in step 4 :
- add a page that says "hello" in arab
- add a link to this page on your landing page

Prepare a release for production with this feature.

## Step 8

Prepare a release for production with the feature added in step 6.